<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Announcement.php';
require_once dirname(__FILE__) . '/classes/Liveshare.php';
require_once dirname(__FILE__) . '/classes/Platform.php';
require_once dirname(__FILE__) . '/classes/Subshare.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$liveDetails = getUser($conn," WHERE broadcast_live = 'Available' AND user_type = '1' ");

$adminShare = getUser($conn," WHERE user_type = '0' ");
$adminData = $adminShare[0];
$adminPlatform = $adminData->getPlatform();
$adminLink = $adminData->getLink();
$adminAutoplay = $adminData->getAutoplay();

$mainAnnoucement = getAnnouncement($conn, " WHERE status = 'Available' AND type = '2' ");
$allAnnoucement = getAnnouncement($conn, " WHERE status = 'Available' AND type = '1' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<title>光明線上產業展 Guang Ming Virtual Expo Centre</title>
<meta property="og:description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="description" content="光明線上產業展 Guang Ming Virtual Expo Centre" />
<meta name="keywords" content="光明線上產業展, Guang Ming Virtual Expo Centre, guang ming, 光明, 光明日报, guang ming daily, virtual expo, 线上产业展, Livestream, Property, video, live, etc">

<?php include 'css.php'; ?>
</head>

<body>

<div class="width100 gold-line"></div>
    <marquee class="announcement-marquee opacity-hover" behavior="scroll" direction="left">
        <a href="" target="_blank">
            <?php
            if($mainAnnoucement)
            {
                for($cntAA = 0;$cntAA < count($mainAnnoucement) ;$cntAA++)
                {
                ?>
                    <?php echo $mainAnnoucement[$cntAA]->getContent();?>
                <?php
                }
            }
            ?>
        </a>
    </marquee>
    <div class="width100 same-padding overflow gold-bg min-height marquee-distance">
    	<div class="width100 overflow text-center">
    		<img src="img/guangminglogo.png" class="guangming-logo" alt="Guang Ming Daily 光明日报" title="Guang Ming Daily 光明日报">
		</div>
        <h1 class="title-h1 text-center landing-title-h1 black-text">光明線上產業展<br>Guang Ming Virtual Expo Centre</h1>
        <!-- <h1 class="title-h1 text-center landing-title-h1 black-text"><?php //echo $mainTitle->getName();?></h1> -->
		<div class="left-video-only-div">

            <div class="width100 overflow">
                <iframe class="landing-top-iframe" src="https://www.youtube.com/embed/<?php echo $adminLink;?>?&playsinline=1&showinfo=0&showsearch=0&rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>

		</div>
   <div class="right-announcement-big-div">
         <h2 class="announcement-title">Announcement</h2>
         
   		<!-- Start repeat php code --->
        <?php
        if($allAnnoucement)
        {
            for($cnt = 0;$cnt < count($allAnnoucement) ;$cnt++)
            {
            ?>
                <a href="" target="_blank">
                    <div class="announcement-per-div opacity-hover">
                        <p class="small-date"><?php echo date("d-m-Y",strtotime($allAnnoucement[$cnt]->getDateCreated()));?></p>
                        <p class="announcement-content">
                            <?php echo $allAnnoucement[$cnt]->getContent();?>
                        </p>
                    
                    </div>
                </a>
            <?php
            }
        }
        ?>
   		<!-- End of php code -->

   </div> 
            
        
        
        
        <div class="two-section-container overflow">
        <?php
        $conn = connDB();
        if($liveDetails)
        {
            for($cnt = 0;$cnt < count($liveDetails) ;$cnt++)
            {
            ?>

                <?php 
                    $platfrom =  $liveDetails[$cnt]->getPlatform();
                    if($platfrom == 'Youtube')
                    {
                    ?>
                        <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>' target="_blank">
                            <div class="two-section-div opacity-hover">
                                <p class="subtitle-p gold-text"><?php echo $liveDetails[$cnt]->getUsername();?></p> 
                                    <iframe class="two-section-iframe" src="https://www.youtube.com/embed/<?php echo $liveDetails[$cnt]->getLink();?>?&playsinline=1&showinfo=0&showsearch=0&rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    <div class="clear"></div>
                                    <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>'><div class="guang-button">View More</div></a>
                            </div>
                        </a>
                    <?php
                    }

                    elseif($platfrom == 'Zoom')
                    {
                    ?>
                        <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>' target="_blank">
							<div class="two-section-div opacity-hover">
                            	<p class="subtitle-p gold-text"><?php echo $liveDetails[$cnt]->getUsername();?></p> 
                                <div class="two-section-iframe background-css" id="z<?php echo $liveDetails[$cnt]->getUid();?>" value="<?php echo $liveDetails[$cnt]->getUid();?>">
                            	</div>
                                <div class="clear"></div>
                                <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>'><div class="guang-button">View More</div></a>
							</div>
                        </a>
                        
						<style>
                        	#z<?php echo $liveDetails[$cnt]->getUid();?>{
								background-image:url(userProfilePic/<?php echo $liveDetails[$cnt]->getBroadcastShare();?>);}
                        </style>
                    <?php
                    }

                    elseif($platfrom == 'Facebook')
                    {
                    ?>

                        <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>' target="_blank">
							<div class="two-section-div opacity-hover">
                            	<p  class="subtitle-p gold-text"><?php echo $liveDetails[$cnt]->getUsername();?></p> 

   
                                    <iframe  class="two-section-iframe" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F?v=<?php echo $liveDetails[$cnt]->getLink();?>"  style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
                           			<div class="clear"></div>
                                    <a href='uniqueIndex.php?id=<?php echo $liveDetails[$cnt]->getUid();?>'><div class="guang-button">View More</div></a>
                            </div>
                        </a>

                    <?php
                    }

                    else
                    {   }
                ?>

            <?php
            }
            ?>

        <?php
        }

        else
        {
        ?>
            NO BROADCASTING AT THE MOMENT, WE WILL RETURN SOON !!
        <?php
        }

        ?>
    </div>


    
    
    </div>

    <div class="clear"></div>
    
</div>

<?php include 'js.php'; ?>

</body>
</html>