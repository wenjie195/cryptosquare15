<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Registration | Property" />
<title>Registration  | Property</title>
<meta property="og:description" content="Property" />
<meta name="description" content="Property" />
<meta name="keywords" content="Livestream, Property, video, live, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<div class="width100 same-padding overflow gold-bg min-height-footer-only">
	<h2 class="h1-title">Registration</h2> 
	<div class="clear"></div>
    <form action="utilities/registrationFunction.php" method="POST" class="margin-top30">
        <div class="dual-input">
            <p class="input-top-text">Username</p>
            <div class="password-input-div">
                <input class="aidex-input clean password-input"  type="text" placeholder="Username" id="register_username" name="register_username" required> 
            </div> 
        </div>  
        <div class="dual-input second-dual-input">
            <p class="input-top-text">Phone Number</p>
            <div class="password-input-div">
                <input class="aidex-input clean password-input"  type="text" placeholder="Phone Number" id="register_phone" name="register_phone" required>
            </div>  
        </div>  
        <div class="clear"></div>
        <div class="width100 overflow text-center">     
            <button class="clean-button clean login-btn pink-button" name="submit">Sign Up</button>
        </div>
    </form>
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>
</body>
</html>