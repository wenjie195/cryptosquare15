<?php
if (session_id() == "")
{
    session_start();
}
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Message.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$messageDetails = getMessage($conn," ORDER BY date_created DESC LIMIT 50 ");
// $messageDetails = getMessage($conn," WHERE id DESC LIMIT 50 ");

if($messageDetails)
{   
    for($cnt = 0;$cnt < count($messageDetails) ;$cnt++)
    {
    ?>
        <div class="admin-chat-bubble"><?php echo $messageDetails[$cnt]->getReceiveSMS();?></div>  
    <?php
    }
    ?>
<?php
}
$conn->close();
?>