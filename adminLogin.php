<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Admin Login | Property" />
<title>Admin Login  | Property</title>
<meta property="og:description" content="Property" />
<meta name="description" content="Property" />
<meta name="keywords" content="Livestream, Property, video, live, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<div class="width100 same-padding overflow gold-bg min-height-footer-only">
	<h2 class="h1-title">Login</h2> 
	<div class="clear"></div>
    <form action="utilities/loginFunction.php" method="POST" class="margin-top30">
            <div class="dual-input">
                <p class="input-top-text">Username</p>
                <div class="password-input-div">
                	<input class="aidex-input clean password-input"  type="text" placeholder="Username" id="username" name="username" required> 
                </div> 
            </div>  
            <div class="dual-input second-dual-input">
                <p class="input-top-text">Password</p>
                <div class="password-input-div">
                    <input class="aidex-input clean password-input"  type="password" placeholder="Password" id="password" name="password" required>
                    <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionC()" alt="View Password" title="View Password">
                </div>  
            </div>  

        <div class="clear"></div>

            <div class="width100 overflow text-center">     
                <button class="clean-button clean login-btn pink-button"   name="login">Login</button>
            </div>

      
    </form>

</div>
<div class="clear"></div>
<?php include 'js.php'; ?>
</body>
</html>