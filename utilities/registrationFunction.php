<?php
if (session_id() == "")
{
     session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Registration.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function registerNewUser($conn,$uid,$username,$phoneNo,$userType)
{
     if(insertDynamicData($conn,"registration",array("uid","username","phone_no","user_type"),
          array($uid,$username,$phoneNo,$userType),"sssi") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $username = rewrite($_POST['register_username']);
     $phoneNo = rewrite($_POST['register_phone']);
     $userType = "1";

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $username."<br>";
     // echo $phoneNo ."<br>";

     $usernameRows = getRegistration($conn," WHERE username = ? ",array("username"),array($username),"s");
     $usernameDetails = $usernameRows[0];

     if (!$usernameDetails)
     {
          if(registerNewUser($conn,$uid,$username,$phoneNo,$userType))
          {
               echo "<script>alert('Register Success !');window.location='../index.php'</script>";   
          }
          else
          {
               echo "<script>alert('fail to register register !');window.location='../registration.php'</script>";   
          } 
     }
     else
     {
          echo "<script>alert('registration data already taken by others !');window.location='../registration.php'</script>";   
     }     
}
else 
{
     header('Location: ../index.php');
}
?>