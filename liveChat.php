<?php
if (session_id() == "")
{
    session_start();
}
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Message.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $messageDetails = getMessage($conn);
$messageDetails = getMessage($conn," ORDER BY date_created DESC LIMIT 50 ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Live Chat | Property" />
<title>Live Chat | Property</title>
<meta property="og:description" content="Property" />
<meta name="description" content="Property" />
<meta name="keywords" content="Livestream, Property, video, live, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<div class="width100 same-padding overflow gold-bg min-height-footer-only">
    <h2 class="h1-title">Live Chat</h2> 

    <div id="divLiveMessage">
    </div>

    <form action="utilities/sentMessageFunction.php" method="POST">
        <input class="aidex-input clean"  type="text" placeholder="Your Message" id="message_details" name="message_details" required>  
        <button class="clean hover1 blue-button smaller-font right-submit" type="submit" name="submit">
            SENT
        </button>
    </form>

</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

<script type="text/javascript">
    $(document).ready(function()
    {
        $("#divLiveMessage").load("liveMessage.php");
    setInterval(function()
    {
        $("#divLiveMessage").load("liveMessage.php");
    }, 5000);
    });
</script>

</body>
</html>